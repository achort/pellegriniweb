var progressbarInterval;

function xmms2_init(json)
{
    update_ui(json);
    
    // playback controls
    $('#playback-prev').on('click', refresh, ajax_link);
    $('#playback-toggle').on('click', refresh, ajax_link);
    $('#playback-stop').on('click', refresh, ajax_link);
    $('#playback-next').on('click', refresh, ajax_link);
    $('#playlist a').on('click', refresh, ajax_link);
}

function update_ui(json) {
    // PLayback information
    $('#server-status').text(json.server.status);
    
    if (json.current == null) {
        $("#progressbar").progressbar({ value: 0 });
    }
    else {
        $('#current').text(json.current.artist + ' - ' + json.current.title);
        $("#progressbar").progressbar({ value: 100 * json.current.playtime / json.current.duration });
        $('#progressbar span').text(get_time_str(json.current.playtime));
    }

    $('#playback-toggle').text(json.server.is_playing ? 'Pause' : 'Play');

    if (json.current != null) {
        $('#playlist td:first-child').text('');
        $('#playlist-item-' + json.current.id + ' td:first-child').text('-->');
    }
    
    // update progress bar without ajax
    var counter = 1;
    progressbarInterval = setInterval(function () {
        if (!json.server.is_playing) {
            return;
        }

        var val = $('#progressbar').progressbar('option', 'value');
        
        val = val * json.current.duration / 100;
        val = 100 * (val + 1000) / json.current.duration;
        if (val > 100) {
            clearInterval(progressbarInterval);
            document.location.reload(true);
        } else {
            $('#progressbar').progressbar({value: val});
            $('#progressbar span').text(get_time_str(json.current.playtime + counter * 1000));
            counter += 1;
        }
    }, 1000);
    
    // Volume slider
    $("#volume").slider({
        value: json.server.volume,
        change: function(event, ui) {
            $.ajax({
                url: "volume/" + ui.value,
                context: document.body
            })
        }
    });
}

function refresh(jsonString) {
    clearInterval(progressbarInterval);
    $('#volume').slider('destroy');
    update_ui($.parseJSON(jsonString));
}

function ajax_link(e) {
    $.ajax({
        url: e.target.href,
        context: document.body,
        success: e.data
    });
    
    return false;
}

function get_time_str(time) {
    var s = time / 1000;
    var min = Math.floor(s / 60);
    var sec = Math.floor(s % 60);
    
    var time_str = min + ':' + sec;
    if (sec < 10) {
        time_str = min + ':0' + sec;
    }
    
    return time_str;
}

/* update progress bar using ajax
var duration = {{ current.duration }};
    $(function() {
        var pGress = setInterval(function() {
            $.ajax({
                    url: "playtime",
                    context: document.body,
                    success: function(data) {
                      $('#progressbar').progressbar('option', 'value', 100 * data / duration);
                    }
            })
        },1000);
    });
*/
