from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('xmmswebclient.views',
    url(r'^$', 'index', name='xmms2-index'),
    url(r'^jump/(?P<index>\d+)/$', 'jumpto', name='xmms2-jumpto'),
    url(r'^prev/$', 'previous', name='xmms2-playback-previous'),
    url(r'^toggle/$', 'toggle', name='xmms2-playback-toggle'),
    url(r'^stop/$', 'stop', name='xmms2-playback-stop'),
    url(r'^next/$', 'next', name='xmms2-playback-next'),
    url(r'^volume/(?P<volume>\d+)/$', 'volume_set', name='xmms2-server-volume'),
    url(r'^playtime/$', 'playtime_get'),
    url(r'^playlist/clear/$', 'playlist_clear', name='xmms2-playlist-clear'),
)
