import xmmsclient
import os

try:
    xmms = xmmsclient.XMMS("lala")
    xmms.connect(os.getenv("XMMS_PATH"))
except IOError:
    os.system("xmms2-launcher")
    xmms = xmmsclient.XMMS("lala")
    xmms.connect(os.getenv("XMMS_PATH"))


class Status:
    Stop, Play, Pause, Down = range(4)


    def __init__(self, xmms_status):
        if xmms_status == xmmsclient.PLAYBACK_STATUS_STOP:
            self.value = self.Stop
        elif xmms_status == xmmsclient.PLAYBACK_STATUS_PLAY:
            self.value = self.Play
        elif xmms_status == xmmsclient.PLAYBACK_STATUS_PAUSE:
            self.value = self.Pause
        else:
            self.value = -1


    def __unicode__(self):
        if self.value == Status.Stop:
            return 'Stopped'
        if self.value == Status.Play:
            return 'Playing'
        if self.value == Status.Pause:
            return 'Paused'
        if self.value == Status.Down:
            return 'Down'

        return 'unkown'

    def is_playing(self):
        return self.value == self.Play


def get_complete_info():
    status = get_status()

    dict = {
        'server': {
            'status': status.__unicode__(),
            'is_playing': status.is_playing(),
            'volume': volume_get()
        },
        'playlist': get_playlist(),
        'current': get_current(),
    }

    return dict


def get_status():
    result = xmms.playback_status()
    result.wait()
    return Status(result.value())


def get_current():
    result = xmms.playback_current_id()
    result.wait()

    current = get_mediainfo(result.value())

    if not current:
        return

    current['playtime'] = get_current_playtime()

    return current


def get_current_playtime():
    result = xmms.playback_playtime()
    result.wait()
    return result.value()


def get_mediainfo(id):
    result = xmms.medialib_get_info(id)
    result.wait()

    minfo = result.value()

    if result.iserror():
        return

    try:
        artist = minfo["artist"]
    except KeyError:
        artist = "No artist"

    try:
        album = minfo["album"]
    except KeyError:
        title = "No album"

    try:
        title = minfo["title"]
    except KeyError:
        title = "No title"

    try:
        bitrate = minfo["bitrate"]
    except KeyError:
        bitrate = 0

    try:
        duration = minfo["duration"]
    except KeyError:
        duration = 0

    return {'id': id, 'artist': artist, 'album':album, 'title': title, 'duration': duration}


def get_playlist():
    result = xmms.playlist_list_entries()
    result.wait()
    lista = []
    for id in result.value():
        lista.append(get_mediainfo(id))

    return lista


def jump_to(index):
    result = xmms.playlist_set_next(index)
    result.wait()
    tickle()
    start_playback()


def jump_to_rel(index):
    result = xmms.playlist_set_next_rel(index)
    result.wait()
    tickle()


def tickle():
    result = xmms.playback_tickle()
    result.wait()


def start_playback():
    result = xmms.playback_start()
    result.wait()


def toggle():
    status = get_status()
    if status.value == Status.Play:
        result = xmms.playback_pause()
    else:
        result = xmms.playback_start()

    result.wait()


def stop():
    result = xmms.playback_stop()
    result.wait()


def volume_get():
    result = xmms.playback_volume_get()
    result.wait()

    if result.iserror():
        return 0

    return result.value()['master']


def volume_set(volume):
    result = xmms.playback_volume_set('left', int(volume))
    result.wait()
    result = xmms.playback_volume_set('right', int(volume))
    result.wait()


def playlist_clear():
    result = xmms.playlist_clear()
    result.wait()

def playlist_add(file):
    xmms.playlist_add_url('file:///media/beta/music/ACDC/AC DC - THE BEST OF AC DC/01 - WHO MADE WHO.mp3')

