import util
import time
import json
from django.template import RequestContext
from django.shortcuts import render_to_response, redirect
from django.http import HttpResponse


def index(request):
    dict = util.get_complete_info()
    dict['json'] = json.dumps(dict)
    
    return render_to_response(
        'xmms2.html',
        dict,
        context_instance=RequestContext(request))


def jumpto(request, index):
    util.jump_to(int(index))
    time.sleep(0.15)
    return HttpResponse(json.dumps(util.get_complete_info()))


def previous(request):
    util.jump_to_rel(-1)
    return HttpResponse(json.dumps(util.get_complete_info()))


def toggle(request):
    util.toggle()
    return HttpResponse(json.dumps(util.get_complete_info()))


def stop(request):
    util.stop()
    return HttpResponse(json.dumps(util.get_complete_info()))


def next(request):
    util.jump_to_rel(1)
    time.sleep(0.15)
    return HttpResponse(json.dumps(util.get_complete_info()))


def playlist_clear(request):
    util.playlist_clear()
    return redirect('xmms2-index')


def volume_set(request, volume):
    util.volume_set(volume)
    return HttpResponse(json.dumps(util.get_complete_info()))


def playtime_get(request):
    return HttpResponse(util.get_current_playtime())


def playlist_add_dir(request):
    return redirect('xmms2-index')
