var template;
var partials;

function filebrowser_init(t, view, p) {
   template = t;
   partials = p;
   $('#lista').html(Mustache.to_html(template, view, partials));
   
   hook_events($('#lista'));
}

function ajax_link(e) {
    $.ajax({
        url: e.target.href,
        context: document.body,
        success: e.data,
        async: false
    });

    return false;
}

function hook_events($root) { 
   $root.find('a.folder-link').on('click', expand_item, toggle_item);
   $root.find('input[type="checkbox"]').on('click', select_subitems);
}

function toggle_item(e) {
  var $target = $(e.target.parentElement);
  var expanded = $target.attr('data-expanded');

  if (expanded == 'true') {
    $target.find('> ul, > li').hide();
  }
  else if (expanded == 'false') {
    $target.find('> ul, > li').show();
  }
  else {
    ajax_link(e); // lookup item content
    $target = $('#' + $target.attr('id')); // refresh target
  }

  $target.attr('data-expanded', expanded != 'true');
  return false;
}

function expand_item(jsonString) {
    var json = $.parseJSON(jsonString);
    var $target = $('#' + json.id);
    var selected = $target.find('>input').attr('checked');

    $target.replaceWith(Mustache.to_html(template, json, template));
    $target = $('#' + json.id); // Refresh $target because of replaceWith.
    $target.find('input').attr('checked', selected);
    hook_events($target);
    return false;
}

function select_subitems(e) {
  $('#' + e.target.parentElement.id + ' input[type="checkbox"]').attr('checked', e.target.checked);
}
