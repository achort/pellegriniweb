from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('filebrowser.views',
    url(r'^$', 'usage', name='filebrowser-usage'),
    url(r'^tree/(?P<path>.+)/$', 'index', name='filebrowser-view'),
    url(r'^get/(?P<path>.+)/$', 'get_folder', name='filebrowser-get')
)
