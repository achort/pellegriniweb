from Crypto.Hash import MD5


def md5(string):
    return MD5.new(string).hexdigest()
