import os
import json
import magic

from django.http import HttpResponse
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.core.urlresolvers import reverse

import util

def index(request, path):
    f = open('filebrowser/tree.mustache')
    template = f.read().replace('\n', '')
    raiz = os.path.join('/', path).encode('utf-8')

    view = {
        'template': template,
        'view': json.dumps(get_tree_small(raiz)),
        'partials': {'subs': template}}

    return render_to_response(
        'filebrowser.html',
        view,
        context_instance=RequestContext(request))


def get_folder(request, path):
    raiz = os.path.join('/', path).encode('utf-8')
    return HttpResponse(json.dumps(get_tree_small(raiz)))


def usage(request):
    return HttpResponse('no path')


def get_tree_small(directorio):
    tree = {
        'id': util.md5(directorio),
        'name': os.path.basename(directorio),
        'type': 'folder',
        'url': reverse(
            'filebrowser-get',
            kwargs={'path': directorio})
     }

    items = sorted([item.decode(errors='replace')
        for item in os.listdir(directorio)])

    tree['folders'] = [{
        'id': util.md5(os.path.join(directorio, item)),
        'name': item,
        'folders': [],
        'files': [],
        'type': 'folder',
        'url': reverse(
            'filebrowser-get',
            kwargs={'path': os.path.join(directorio, item)})}
        for item in items if os.path.isdir(os.path.join(directorio, item))]

    tree['files'] = [{
        'id': util.md5(os.path.join(directorio, item)),
        'name': item,
        'type': magic.from_file(os.path.join(directorio, item))}
        for item in items if os.path.isfile(os.path.join(directorio, item))]

    return tree


def get_tree_full(directorio):
    tree = {"id": directorio}
    files_temp = {}
    folders_temp = {}

    for root, dirs, files in os.walk(directorio, False):
        files_temp[root] = []
        folders_temp[root] = []
        for file in files:
            id = os.path.join(root, file).decode(errors='replace')
            files_temp[root].append({
                "id": id,
                "title": file.decode(errors='replace'),
                "url": reverse('filebrowser-get', kwargs={'path': id})})

        for dir in [dir for dir in dirs if os.path.join(root, dir) in folders_temp]:
            folders_temp[root].append({
                "id": os.path.join(root, dir).decode(errors='replace'),
                "title": dir.decode(errors='replace'),
                "folders": folders_temp[os.path.join(root, dir)],
                "files": files_temp[os.path.join(root, dir)]})

    tree["folders"] = folders_temp[directorio]
    tree["files"] = files_temp[directorio]

    return tree
