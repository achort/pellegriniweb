from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    url(r'^xmms2/', include('xmmswebclient.urls')),
    url(r'^filebrowser/', include('filebrowser.urls')),
)
